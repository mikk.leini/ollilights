/*
 * effects.h
 *
 *  Created on: 10. jaan 2018
 *      Author: Mikk
 */

#ifndef EFFECTS_H_
#define EFFECTS_H_

extern void effects_Init();
extern void effects_Next();
extern void effects_Run();

#endif /* EFFECTS_H_ */
