/*
 * leds.h
 *
 *  Created on: 10. jaan 2018
 *      Author: Mikk
 */

#ifndef LEDS_H_
#define LEDS_H_

#include "main.h"


#define WIDTH     6
#define HEIGHT   14
#define TONES     8


extern void leds_Init();
extern void leds_UnInit();
extern void leds_DrawPixel(int32_t x, int32_t y, uint32_t tone);
extern void leds_SwapPage();

#endif /* LEDS_H_ */
