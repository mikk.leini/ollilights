/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H__
#define __MAIN_H__

/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx.h"
#include "stm32l0xx_ll_system.h"
#include "stm32l0xx_ll_gpio.h"
#include "stm32l0xx_ll_exti.h"
#include "stm32l0xx_ll_bus.h"
#include "stm32l0xx_ll_cortex.h"
#include "stm32l0xx_ll_rcc.h"
#include "stm32l0xx_ll_utils.h"
#include "stm32l0xx_ll_pwr.h"
#include "stm32l0xx_ll_dma.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define BTN2_Pin LL_GPIO_PIN_13
#define BTN2_GPIO_Port GPIOC
#define Y15_Pin LL_GPIO_PIN_14
#define Y15_GPIO_Port GPIOC
#define Y14_Pin LL_GPIO_PIN_15
#define Y14_GPIO_Port GPIOC
#define Y13_Pin LL_GPIO_PIN_0
#define Y13_GPIO_Port GPIOH
#define Y12_Pin LL_GPIO_PIN_1
#define Y12_GPIO_Port GPIOH
#define BTN1_Pin LL_GPIO_PIN_0
#define BTN1_GPIO_Port GPIOA
#define Y11_Pin LL_GPIO_PIN_2
#define Y11_GPIO_Port GPIOA
#define Y10_Pin LL_GPIO_PIN_3
#define Y10_GPIO_Port GPIOA
#define Y9_Pin LL_GPIO_PIN_4
#define Y9_GPIO_Port GPIOA
#define Y8_Pin LL_GPIO_PIN_5
#define Y8_GPIO_Port GPIOA
#define Y7_Pin LL_GPIO_PIN_6
#define Y7_GPIO_Port GPIOA
#define X7_Pin LL_GPIO_PIN_7
#define X7_GPIO_Port GPIOA
#define Y6_Pin LL_GPIO_PIN_0
#define Y6_GPIO_Port GPIOB
#define X6_Pin LL_GPIO_PIN_1
#define X6_GPIO_Port GPIOB
#define Y5_Pin LL_GPIO_PIN_2
#define Y5_GPIO_Port GPIOB
#define X5_Pin LL_GPIO_PIN_10
#define X5_GPIO_Port GPIOB
#define Y4_Pin LL_GPIO_PIN_11
#define Y4_GPIO_Port GPIOB
#define X4_Pin LL_GPIO_PIN_12
#define X4_GPIO_Port GPIOB
#define Y3_Pin LL_GPIO_PIN_13
#define Y3_GPIO_Port GPIOB
#define X3_Pin LL_GPIO_PIN_14
#define X3_GPIO_Port GPIOB
#define Y2_Pin LL_GPIO_PIN_15
#define Y2_GPIO_Port GPIOB
#define X2_Pin LL_GPIO_PIN_8
#define X2_GPIO_Port GPIOA
#define Y1_Pin LL_GPIO_PIN_9
#define Y1_GPIO_Port GPIOA
#define X1_Pin LL_GPIO_PIN_10
#define X1_GPIO_Port GPIOA
#define Y0_Pin LL_GPIO_PIN_11
#define Y0_GPIO_Port GPIOA
#define X0_Pin LL_GPIO_PIN_12
#define X0_GPIO_Port GPIOA
#define LED1_Pin LL_GPIO_PIN_8
#define LED1_GPIO_Port GPIOB

/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
/* #define USE_FULL_ASSERT    1U */

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
