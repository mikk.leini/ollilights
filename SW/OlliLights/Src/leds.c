/*
 * leds.c
 *
 *  Created on: 10. jaan 2018
 *      Author: Mikk
 */

#include "main.h"
#include "leds.h"


static uint32_t leds_image[2][HEIGHT][WIDTH];
static uint32_t leds_page;
static uint32_t x, y, bit;

extern TIM_HandleTypeDef htim2;

/**
 * LEDs initialization
 */
void leds_Init()
{
  x = 0;
  y = 0;
  bit = 0;
  leds_page = 0;

  /* TIM2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(TIM2_IRQn, 2, 0);
  HAL_NVIC_EnableIRQ(TIM2_IRQn);

  /* Start TIM2 */
  HAL_TIM_Base_Start_IT(&htim2);
}

/**
 * LEDs uninitialization
 */
void leds_UnInit()
{
  HAL_TIM_Base_DeInit(&htim2);
  HAL_NVIC_DisableIRQ(TIM2_IRQn);
}

/**
 * Drawing one pixel with LEDs
 */
void leds_DrawPixel(int32_t x, int32_t y, uint32_t tone)
{
  if ((x >= 0) && (x < WIDTH) && (y >= 0) && (y < HEIGHT))
  {
    if (tone > TONES) tone = TONES;

    leds_image[leds_page][y][x] = (1 << tone) - 1;
  }
}

/**
 * Swapping LED's page
 */
void leds_SwapPage()
{
  leds_page = 1 - leds_page;
}

// X - anode   (plus)
// Y - cathode (minus)

#define CONTROL_X_OFF(n) case n:            LL_GPIO_ResetOutputPin(X ## n ## _GPIO_Port, X ## n ## _Pin); break;
#define CONTROL_X_ON(n)  case n: if (onoff) LL_GPIO_SetOutputPin  (X ## n ## _GPIO_Port, X ## n ## _Pin); break;
#define CONTROL_Y_OFF(n) case n:            LL_GPIO_SetOutputPin  (Y ## n ## _GPIO_Port, Y ## n ## _Pin); break;
#define CONTROL_Y_ON(n)  case n:            LL_GPIO_ResetOutputPin(Y ## n ## _GPIO_Port, Y ## n ## _Pin); break;


/**
 * LEDs interrupt
 */
void TIM2_IRQHandler()
{
  uint32_t onoff;

  HAL_NVIC_ClearPendingIRQ(TIM2_IRQn);
  __HAL_TIM_CLEAR_IT(&htim2, TIM_IT_UPDATE);

  switch (x)
  {
    CONTROL_X_OFF(0)
    CONTROL_X_OFF(1)
    CONTROL_X_OFF(2)
    CONTROL_X_OFF(3)
    CONTROL_X_OFF(4)
    CONTROL_X_OFF(5)
  }

  x++;

  if (x >= WIDTH)
  {
    x = 0;

    switch (y)
    {
      CONTROL_Y_OFF(0)
      CONTROL_Y_OFF(1)
      CONTROL_Y_OFF(2)
      CONTROL_Y_OFF(3)
      CONTROL_Y_OFF(4)
      CONTROL_Y_OFF(5)
      CONTROL_Y_OFF(6)
      CONTROL_Y_OFF(7)
      CONTROL_Y_OFF(8)
      CONTROL_Y_OFF(9)
      CONTROL_Y_OFF(10)
      CONTROL_Y_OFF(11)
      CONTROL_Y_OFF(12)
      CONTROL_Y_OFF(13)
    }

    y++;

    if (y >= HEIGHT)
    {
      y = 0;

      bit = (bit + 1) % TONES;
    }

    switch (y)
    {
      CONTROL_Y_ON(0)
      CONTROL_Y_ON(1)
      CONTROL_Y_ON(2)
      CONTROL_Y_ON(3)
      CONTROL_Y_ON(4)
      CONTROL_Y_ON(5)
      CONTROL_Y_ON(6)
      CONTROL_Y_ON(7)
      CONTROL_Y_ON(8)
      CONTROL_Y_ON(9)
      CONTROL_Y_ON(10)
      CONTROL_Y_ON(11)
      CONTROL_Y_ON(12)
      CONTROL_Y_ON(13)
    }
  }

  onoff = (leds_image[leds_page][y][x] >> bit) & 1;

  switch (x)
  {
    CONTROL_X_ON(0)
    CONTROL_X_ON(1)
    CONTROL_X_ON(2)
    CONTROL_X_ON(3)
    CONTROL_X_ON(4)
    CONTROL_X_ON(5)
  }
}
