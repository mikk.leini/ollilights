#include <stdlib.h>
#include "leds.h"

#define NUM_STARS   10
#define STARS_TIME  100

typedef enum
{
  STARS_IDLE,
  STARS_APPEAR,
  STARS_STAY,
  STARS_DISAPPEAR
} StarState;

typedef struct
{
    StarState state;
    int32_t x, y;
    uint32_t t;
    uint32_t timer;
} Star;

static Star stars[NUM_STARS];

/**
 * Initialize effect
 */
void stars_Init()
{
  uint32_t x, y;
  uint32_t i;

  for (y = 0; y < HEIGHT; y++)
  {
    for (x = 0; x < WIDTH; x++)
    {
      leds_DrawPixel(x, y, 0);
    }
  }

  for (i = 0; i < NUM_STARS; i++)
  {
    stars[i].state = STARS_IDLE;
    stars[i].timer = rand() % 200;
  }
}

/**
 * Draw effect
 */
void stars_Draw()
{
  uint32_t i;
  Star * star;

  for (i = 0; i < NUM_STARS; i++)
  {
    star = &stars[i];

    switch (star->state)
    {
      case STARS_IDLE:
        if (star->timer == 0)
        {
          star->x = rand() % WIDTH;
          star->y = rand() % HEIGHT;
          star->t = 0;

          star->state = STARS_APPEAR;
        }
        else
        {
          star->timer--;
        }
        break;

      case STARS_APPEAR:

        star->t++;
        leds_DrawPixel(star->x, star->y, star->t / 20);

        if (star->t == (TONES * 20 + 20))
        {
          star->state = STARS_STAY;
          star->timer = 20 + rand() % 100;
        }
        break;

      case STARS_STAY:
        if (star->timer == 0)
        {
          star->state = STARS_DISAPPEAR;
        }
        else
        {
          star->timer--;
        }
        break;

      case STARS_DISAPPEAR:

        star->t--;
        leds_DrawPixel(star->x, star->y, star->t / 20);

        if (star->t == 0)
        {
          star->timer = 20 + rand() % 200;
          star->state = STARS_IDLE;
        }
        break;
    }
  }
}
