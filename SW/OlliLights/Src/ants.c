#include <stdlib.h>
#include "main.h"
#include "leds.h"

#define NUM_ANTS      2
#define SPEED         50

#define MASK(b) (1 << (b))

#define UP    0
#define RIGHT 1
#define DOWN  2
#define LEFT  3

typedef struct
{
  int32_t x, y;
  uint32_t len;
  uint32_t dir;
  uint32_t timer;
} Ants;

static Ants ants[NUM_ANTS];

static void checkPossibleDirection(Ants * ant);
static uint32_t randomNewDir(Ants * ant, uint32_t possibilites);

/**
 * Initialize effect
 */
void ants_Init()
{
  uint32_t x, y;
  uint32_t i;

  for (y = 0; y < HEIGHT; y++)
  {
    for (x = 0; x < WIDTH; x++)
    {
      leds_DrawPixel(x, y, 0);
    }
  }

  for (i = 0; i < NUM_ANTS; i++)
  {
    ants[i].x = 1 + (rand() % (WIDTH - 2));
    ants[i].y = 1 + (rand() % (HEIGHT - 2));
    ants[i].dir = rand() % 4;
    ants[i].len = 2 + (rand() % 5);
  }
}

/**
 * Draw effect
 */
void ants_Draw()
{
  uint32_t i;
  Ants * ant;

  for (i = 0; i < NUM_ANTS; i++)
  {
    ant = &ants[i];

    if (ant->timer-- > 0) continue;
    ant->timer = SPEED;

    /* Check for possible movement direction */
    checkPossibleDirection(ant);

    /* Clear old position */
    leds_DrawPixel(ant->x, ant->y, 0);

    /* Make the move */
    switch (ant->dir)
    {
      case UP:    ant->y--; break;
      case RIGHT: ant->x++; break;
      case DOWN:  ant->y++; break;
      case LEFT:  ant->x--; break;
    }

    /* Draw ant on new position */
    leds_DrawPixel(ant->x, ant->y, TONES);
  }
}

static void checkPossibleDirection(Ants * ant)
{
  uint32_t dir = ant->dir;

  if ((ant->x <= 0) && (ant->y <= 0)) /* Top left corner */
  {
    dir = randomNewDir(ant, MASK(RIGHT) | MASK(DOWN));
  }
  else if ((ant->x >= (WIDTH - 1)) && (ant->y <= 0)) /* Top right corner */
  {
    dir = randomNewDir(ant, MASK(LEFT) | MASK(DOWN));
  }
  else if ((ant->x >= (WIDTH - 1)) && (ant->y >= (HEIGHT - 1))) /* Bottom right corner */
  {
    dir = randomNewDir(ant, MASK(LEFT) | MASK(UP));
  }
  else if ((ant->x <= 0) && (ant->y >= (HEIGHT - 1))) /* Bottom left corner */
  {
    dir = randomNewDir(ant, MASK(RIGHT) | MASK(UP));
  }
  else if ((ant->y <= 0) && ((ant->dir == 0) || (ant->len == 0))) /* Top wall */
  {
    dir = randomNewDir(ant, MASK(LEFT) | MASK(RIGHT) | MASK(DOWN));
  }
  else if ((ant->x >= (WIDTH - 1)) && ((ant->dir == 1) || (ant->len == 0))) /* Right wall */
  {
    dir = randomNewDir(ant, MASK(LEFT) | MASK(UP) | MASK(DOWN));
  }
  else if ((ant->y >= (HEIGHT - 1)) && ((ant->dir == 2) || (ant->len == 0))) /* Bottom wall */
  {
    dir = randomNewDir(ant, MASK(LEFT) | MASK(RIGHT) | MASK(UP));
  }
  else if ((ant->x <= 0) && ((ant->dir == 3) || (ant->len == 0))) /* Left wall */
  {
    dir = randomNewDir(ant, MASK(RIGHT) | MASK(UP) | MASK(DOWN));
  }
  else
  {
    if (ant->len > 0)
    {
      /* Keep the direction, subtract distance */
      ant->len--;
    }
    else
    {
      /* Pick new direction which is left or right turn */
      dir = randomNewDir(ant, MASK(UP) | MASK(DOWN) | MASK(LEFT) | MASK(RIGHT));
    }
  }

  /* New direction ? Then pick random movement distance */
  if (dir != ant->dir)
  {
    ant->dir = dir;
    ant->len = 2 + (rand() % 5);
  }
}

static uint32_t randomNewDir(Ants * ant, uint32_t possibilites)
{
  uint32_t dir;

  while (1)
  {
    /* Pick new direction which is not the same direction */
    dir = (ant->dir + 1 + (2 * (rand() % 2))) % 4;

    /* Now make sure this move is possible */
    if ((possibilites & MASK(dir)) != 0) break;
  }

  return dir;
}
