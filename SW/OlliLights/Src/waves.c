#include <stdlib.h>
#include "main.h"
#include "leds.h"

static int wave;

/**
 * Initialize effect
 */
void waves_Init()
{
  uint32_t x, y;

  wave = 0;

  for (y = 0; y < HEIGHT; y++)
  {
    for (x = 0; x < WIDTH; x++)
    {
      leds_DrawPixel(x, y, 0);
    }
  }
}

/**
 * Draw effect
 */
void waves_Draw()
{
  uint32_t x, y;
  uint32_t i;

  wave = (wave + 1) % HEIGHT;

  for (y = 0; y < HEIGHT; y++)
  {
    for (x = 0; x < WIDTH; x++)
    {
      if (y == wave) i = TONES;
      else if (y == ((wave - 1) % HEIGHT)) i = 2;
      else if (y == ((wave + 1) % HEIGHT)) i = 2;
      else i = 0;

      leds_DrawPixel(x, y, i);
    }
  }
}
