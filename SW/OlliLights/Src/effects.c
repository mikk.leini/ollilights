/*
 * effects.c
 *
 *  Created on: 10. jaan 2018
 *      Author: Mikk
 */


#include <stdlib.h>
#include "main.h"
#include "leds.h"
#include "stars.h"
#include "waves.h"
#include "comets.h"
#include "ants.h"

static int effect;

struct
{
  void (* init)(void);
  void (* draw)(void);

} effects_Table[] =
{
  { stars_Init,  stars_Draw  }
/*
  { comets_Init, comets_Draw },
  { ants_Init,   ants_Draw   }*/
};

#define NUM_EFFECTS  (sizeof(effects_Table) / sizeof(effects_Table[0]))

/**
 * Initialize effects
 */
void effects_Init()
{
  effect = 0;

  effects_Table[effect].init();
}

/**
 * Next effect selection
 */
void effects_Next()
{
  effect = (effect + 1) % NUM_EFFECTS;

  effects_Table[effect].init();
}

/**
 * Running effects
 */
void effects_Run()
{
  effects_Table[effect].draw();
}
