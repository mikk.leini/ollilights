#include <stdlib.h>
#include "main.h"
#include "leds.h"

#define NUM_COMETS    2
#define COMET_TIME  100
#define SPEED         4

typedef struct
{
  uint32_t active;
  int32_t x, y;
  int32_t dir;
  int32_t yd;
  uint32_t timer;
} Comet;

static Comet comets[NUM_COMETS];

/**
 * Initialize effect
 */
void comets_Init()
{
  uint32_t x, y;
  uint32_t i;

  for (y = 0; y < HEIGHT; y++)
  {
    for (x = 0; x < WIDTH; x++)
    {
      leds_DrawPixel(x, y, 0);
    }
  }

  for (i = 0; i < NUM_COMETS; i++)
  {
    comets[i].active = 0;
    comets[i].timer = 10 + (rand() % 200);
  }
}

/**
 * Draw effect
 */
void comets_Draw()
{
  uint32_t i, j;
  uint32_t traj_ok;
  Comet * comet;

  for (i = 0; i < NUM_COMETS; i++)
  {
    comet = &comets[i];

    if (comet->active == 0)
    {
       if (comet->timer == 0)
       {
         /* Find a trajectory which does not match with other coments */
         do
         {
           comet->x = rand() % WIDTH;
           traj_ok = 1;

           for (j = 0; j < NUM_COMETS; j++)
           {
             if ((j != i) && (comets[j].x == comet->x) && (comets[j].active))
             {
               traj_ok = 0;
               break;
             }
           }
         } while (!traj_ok);

         /* Pick a random movement direction */
         comet->dir = ((rand() % 2) == 0 ? -1 : +1);

         if (comet->dir > 0)
         {
           comet->yd = 0;
         }
         else
         {
           comet->yd = HEIGHT * SPEED;
         }

         comet->y = comet->yd / SPEED;

         /* Active now */
         comet->active = 1;
       }
       else
       {
         comet->timer--;
       }
    }
    else
    {
      leds_DrawPixel(comet->x, comet->y, TONES);
      leds_DrawPixel(comet->x, comet->y - 1 * comet->dir, TONES / 3);
      leds_DrawPixel(comet->x, comet->y - 2 * comet->dir, 0);

      comet->yd += comet->dir;
      comet->y = comet->yd / SPEED;

      if ((comet->y < -3) || (comet->y > HEIGHT + 3))
      {
        comet->active = 0;
        comet->timer = 50 + rand() % 200;
      }
    }
  }
}
