It's 16x8 LED matrix project with STM32L031K6T6 MCU. It simulates he starry sky above the childrens bed.

It works on 3xAA batteries. Sleep current is 27 uA. Batteries have lasted for 1 year now.

Start it from button press and it will light up in 3 seconds (to avoid child seeing the hidden button when you press ;)). It turns off in 30 minutes or turn it off with long press.
